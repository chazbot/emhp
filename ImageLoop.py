# Copyright 2017 The Scripps Research Institute

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## ImageLoop.py
## Basic class for image iteration and processing.
## bowman@scripps.edu

import os, sys, argparse, types, copy_reg, time
from multiprocessing import Pool

from Utilities import print_progress, print_error

def _pickle_method(method):
	if method.im_self is None:
		return getattr, (method.im_class, method.im_func.func_name)
	else:
		return getattr, (method.im_self, method.im_func.func_name)

copy_reg.pickle(types.MethodType, _pickle_method)

class ImageLoop(object):

	#ImageLoop global init
	def __init__(self):
		
		#Initialize loop values
		self.images = []
		self.complete = set()
		self.total = -1
		self.count = -1

	def parse_args(self):
		parser = argparse.ArgumentParser(description='EMHP Image Processing .999\nby Zachary Berndsen, Haerin Jang, and Charles Bowman\n Ward Lab - TSRI')

		#Required Arguments
		required = parser.add_argument_group("Required Arguments")
		required.add_argument('--image_list', dest='imageList', metavar='PATH', help='Path to list of images to run autoMasker on (images should have absolute paths)')

		#Optional Arguments
		optionalArgs = parser.add_argument_group("Optional Arguments")
		optionalArgs.add_argument('--log_path', default='', dest='logPath', metavar='PATH', help='Path to progress and log files, default is current folder')
		optionalArgs.add_argument('--resume', action='store_true', help='Resume masking')
		optionalArgs.add_argument('--multi', default=1, metavar='INT', type=int, help='Number of images to process concurrently.')
		
		#Pick Arguments
		pickArgs = parser.add_argument_group("Particle Pick Filtering Options")
		pickArgs.add_argument('--filter_picks', dest='pickSuffix', metavar='SUFFIX', help='Suffix of pick file in same location as image file')
		pickArgs.add_argument('--save_suffix', dest='saveSuffix', metavar='SUFFIX', help='optional suffix to use for filtered pick lists (default _picks_filtered.txt/star)')
		pickArgs.add_argument('--raw', action='store_true', help='Pick files are raw pick results (column 1 and 2 are coordinates, eman .box files)')
		pickArgs.add_argument('--appion_DoG', action='store_true', help='Pick files are from Appion integrated DoG picking')
		pickArgs.add_argument('--appion_template', action='store_true', help='Pick files are from Appion integrated template picking')
		pickArgs.add_argument('--raw_relion', action='store_true', help='Pick files are from RELION template picker')
		pickArgs.add_argument('--write_jpg', dest='writeImage', action='store_true', help='Write summary JPG after masking and filtering picks to survey finished product')
		pickArgs.add_argument('--box', metavar='FLOAT', type=float, help='Box size in Pixels')

		return parser, required, optionalArgs, pickArgs

	def init_vars(self, *args, **kwargs):
		#Global Args
		#Required
		self.imageList = kwargs.get('imageList')

		#Optional
		self.resume = kwargs.get('resume', False)
		self.logPath = kwargs.get('logPath', '')
		self.logName = 'loop_log.txt'

		#initialize threading
		self.multi = kwargs.get('multi', 1)
		if self.multi > 1:
			self.maskPool = Pool(processes=self.multi)
			self.maskJobs = []

		#All subclasses should make a self.logFile
		self.logFile = os.path.join(self.logPath, self.logName)

		# Pick Parameters
		self.pickSuffix = kwargs.get('pickSuffix', None)

		self.saveSuffix = kwargs.get('saveSuffix', '_picks_filtered.txt')
		if not self.saveSuffix:
			self.saveSuffix = '_picks_filtered.txt'

		if self.pickSuffix == self.saveSuffix:
			print "Input pick suffix and output pick suffix are identical..."
			print "Please change this - I don't want to erase your original picks!"
			sys.exit(0)

		self.box = kwargs.get('box', 0)
		if kwargs.get('raw'):
			self.pickInput = 'raw'
		elif kwargs.get('appion_DoG'):
			self.pickInput = 'apDog'
		elif kwargs.get('appion_template'):
			self.pickInput = 'apTemplate'
		elif kwargs.get('raw_relion'):
			self.pickInput = 'rln'
		else:
			self.pickInput = None
		self.writeImage = kwargs.get('writeImage', False)



	def init_loop(self):

		#Look for the file to open
		if os.path.isfile(str(self.imageList)):
			with open(str(self.imageList), 'r') as f:
				self.images = f.read().splitlines()
				f.close()
				self.total = len(self.images)
				self.count = 0
				if self.total < 1:
					print_error("Your image list is empty.")
				if self.resume:
					with open(str(self.logFile), 'r') as f:
						self.complete = set(f.read().splitlines())
						f.close()
				else:
					self.complete = set()
					open(self.logFile, 'w').close()
		else:
			print_error("Could not open image list.")

	def process_images(self):

		print_progress(self.count, self.total, prefix = 'Processing Images:', suffix = '', barLength = 50)

		if self.multi > 1:
			procs = self.maskPool.imap_unordered(self.process_image, self.images)
			self.maskPool.close()
			while (True):
				completed = procs._index
				if (completed == self.total): break
				print_progress(completed, self.total, prefix = 'Processing Images:', suffix = '', barLength = 50)
				time.sleep(.5)
			print_progress(completed, self.total, prefix = 'Processing Images:', suffix = '', barLength = 50)
			return

		while self.count < self.total:

			if self.count < 0:
				self.count = 0

			imagePath = self.images[self.count]
			imagePathBase = os.path.splitext(imagePath)[0]


			if self.resume and imagePathBase in self.complete:
				self.complete.discard(imagePathBase)
				self.count += 1
				print_progress(self.count, self.total, prefix = 'Processing Images:', suffix = '', barLength = 50)
				continue
			else:
				processed = self.process_image(imagePath)
				
				if processed:
					self.count += 1
					print_progress(self.count, self.total, prefix = 'Processing Images:', suffix = '', barLength = 50)
					open(self.logFile, 'a').write(imagePathBase + '\n')
				else:
					self.count -= 1
					print_progress(self.count, self.total, prefix = 'Processing Images:', suffix = '', barLength = 50)
				continue

	def process_image(self, paramImagePath):
		if not os.path.isfile(paramImagePath):
			print_error("\n%s is not found, check your image lists." % paramImagePath)

	def __getstate__(self):
		self_dict = self.__dict__.copy()
		del self_dict['maskPool']
		del self_dict['maskJobs']
		return self_dict

# End of File
