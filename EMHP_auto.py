# Copyright 2017 The Scripps Research Institute

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## EMHP_auto.py
## Mask holes in cryoEM images and filter pick files based on masks - automatically
## bowman@scripps.edu

import os, sys, argparse
from math import ceil, sqrt

# Thanks for the parser AMI 8)
from pyami import mrc

import numpy as np
from PIL import Image
from scipy.ndimage.filters import gaussian_filter
from skimage.exposure import rescale_intensity
from skimage.filters import sobel
#from scipy.misc import imsave

from Utilities import *
from ImageLoop import ImageLoop
diag = False

class AutoMasker(ImageLoop):

	def __init__(self):
		ImageLoop.__init__(self)

		# Speed vs Accuracy Globals, set as constants
		self.circle_stepsize = 2
		self.low_perc = 1
		self.high_perc = 99

	def parse_args(self):
		parser, required, optionalArgs, pickArgs = super(AutoMasker, self).parse_args()

		required.add_argument('--hole_diameter', metavar='INTEGER', type=int, help='Diameter of hole in carbon grid in Angstroms')
		required.add_argument('--apix', metavar='FLOAT', type=float, help='Pixel size in Angstroms')

		optionalArgs.add_argument('--sigma', default=5, metavar='INTEGER', type=int, help='Diameter of Gaussian Filter in preprocessing DEFAULT 5')
		optionalArgs.add_argument('--smoothing_radius', default=10, metavar='INTEGER', type=int, help='radius used as a factor in radial averaging DEFAULT 10')
		optionalArgs.add_argument('--threshold1', default=.2, metavar='0.0-1.0', type=float, help='Threshold step 1 cutoff DEFAULT .2')
		optionalArgs.add_argument('--threshold2', default=.6, metavar='0.0-1.0', type=float, help='Threshold step 2 cutoff DEFAULT .6')
		optionalArgs.add_argument('--pixel_shift', default=100, metavar='INTEGER', type=int, help='Distance to shift final mask in pixels DEFAULT 100')

		args = parser.parse_args()
		return parser, args

	def init_vars(self, *args, **kwargs):
		super(AutoMasker, self).init_vars(*args, **kwargs)

		# Required Parameters
		self.hole_diameter = kwargs.get('hole_diameter')
		self.apix = kwargs.get('apix')

		# Optional Parameters
		self.sigma = kwargs.get('sigma', 5)
		self.radial_radius = kwargs.get('smoothing_radius', 10)
		self.threshold1 = kwargs.get('threshold1',.2)
		self.threshold2 = kwargs.get('threshold2',.6)
		self.pixel_shift = kwargs.get('pixel_shift', 100)

		# Override log name
		self.logName = 'AutoMasker_log.txt'
		self.logFile = os.path.join(self.logPath, self.logName)

	def process_image(self, paramImagePath):

		#get base image path
		imagePathBase = os.path.splitext(paramImagePath)[0]

		#Add check for missing images
		super(AutoMasker, self).process_image(paramImagePath)
		
		####
		# Hole Finding Method
		####

		#Load the image using EMAN2 tools
		im = mrc.read(paramImagePath)

		#gaussian_filter
		im = gaussian_filter(im, sigma=self.sigma)

		#10x downsample
		im_10 = im[0::10,0::10]

		if diag:
			im_diag = Image.fromarray(np.uint8(im_10))
			im_diag.save(imagePathBase + "_00_gaus_dec.jpg")

		#enhance contrast
		p1, p99 = np.percentile(im_10, (self.low_perc, self.high_perc))
		im_10 = rescale_intensity(im_10, in_range=(p1, p99))

		if diag:
			im_diag = Image.fromarray(np.uint8(im_10 * 255))
			im_diag.save(imagePathBase + "_01_contrast.jpg")

		#normalization, edge detection
		im_10_max = np.max(im_10)
		im_10 = im_10 / (im_10_max)
		im_10 = sobel(im_10)
		im_10 = im_10 > .2 

		if diag:
			im_diag = Image.fromarray(np.uint8(im_10 * 255))
			im_diag.save(imagePathBase + "_02_sobel.jpg")

		#radial summing
		im_10_sum1 = radial_sum(im_10, self.radial_radius)

		#Normalize and threshold1
		im_10_sum1_max = np.max(im_10_sum1)
		im_10_sum1 = im_10_sum1 / (im_10_sum1_max)
		im_10_sum1[im_10_sum1 < self.threshold1] = 0

		if diag:
			im_diag = Image.fromarray(np.uint8(im_10_sum1 * 255))
			im_diag.save(imagePathBase + "_03_thresh1.jpg")

		#radial summing2
		im_10_sum2 = radial_sum(im_10_sum1, self.radial_radius)

		#Normalize and threshold2
		im_10_sum2_max = np.max(im_10_sum2)
		im_10_sum2 = im_10_sum2 / (im_10_sum2_max)
		im_10_sum2 = im_10_sum2 > self.threshold2

		if diag:
			im_diag = Image.fromarray(np.uint8(im_10_sum2 * 255))
			im_diag.save(imagePathBase + "_04_thresh2.jpg")

		#start fit_circle
		rad = int((self.hole_diameter/self.apix)/2)

		rad_10 = int((self.hole_diameter/self.apix)/20)
		shape_10 = [im_10_sum2.shape[0] + (2 * rad_10),
					im_10_sum2.shape[1] + (2 * rad_10)]

		im_100 = im_10_sum2[0::10,0::10]

		rad_100 = int((self.hole_diameter/self.apix)/200)
		shape_100 = [im_100.shape[0] + (2 * rad_100),
					im_100.shape[1] + (2 * rad_100)]

		#See which quadrant is the biggest
		quadrants = {'TL':np.sum(im_100[:im_100.shape[0]/2,:im_100.shape[1]/2]),
					 'TR':np.sum(im_100[:im_100.shape[0]/2,im_100.shape[1]/2:]),
					 'BL':np.sum(im_100[im_100.shape[0]/2:,:im_100.shape[1]/2]),
					 'BR':np.sum(im_100[im_100.shape[0]/2:,im_100.shape[1]/2:])}
		max_quadrant = max(quadrants, key=quadrants.get)

		#store [[xGT, xLT],[yGT, yLT]] for each quadrant
		quadrant_coord = {'TL':[[0,shape_100[0]/2],[0,shape_100[1]/2]],'TR':[[shape_100[0]/2,shape_100[0]],[0,shape_100[1]/2]],'BL':[[0,shape_100[0]/2],[shape_100[1]/2,shape_100[1]]],'BR':[[shape_100[0]/2,shape_100[0]],[shape_100[1]/2,shape_100[1]]]}

		max_coord = (0,0)
		max_overlap = 0
		overlaps = []

		t_c = 0
		t_s = 0

		for c_x in range(0,shape_100[0],self.circle_stepsize):
			for c_y in range(0,shape_100[1],self.circle_stepsize):

				# Skip impossible quadrants
				if quadrant_coord[max_quadrant][0][0] <= c_x < quadrant_coord[max_quadrant][0][1] and quadrant_coord[max_quadrant][1][0] <= c_y < quadrant_coord[max_quadrant][1][1]:
					t_s += 1
					continue
				t_c += 1

				g_100_x, g_100_y = np.meshgrid(np.arange(im_100.shape[1]),np.arange(im_100.shape[0]))
				distance = np.floor(np.sqrt((g_100_x + rad_100 - c_x)**2 + (g_100_y + rad_100 -c_y)**2))

				rows, cols = np.where(np.logical_and(rad_100 == distance, im_100))

				overlap = len(rows)
				overlaps.append(overlap)
				if overlap > max_overlap:
					max_overlap = overlap
					max_coord = [c_x,c_y]

		g_x,g_y = np.meshgrid(np.arange(im.shape[1]),np.arange(im.shape[0]))
		imageMask = np.transpose(((g_x + rad - max_coord[0]*100)**2 + (g_y + rad - max_coord[1]*100)**2) < (rad - self.pixel_shift)**2)
		
		####
		# End Hole Finding Method
		####
		
		#Save Mask file
		# Fix for numpy 1.14 Bug:
		result = Image.fromarray((imageMask * 255).astype(np.uint8))
		result.save(imagePathBase + '_mask.png')
		#imsave(imagePathBase + '_mask.png', imageMask)

		if self.pickSuffix is not None and self.pickInput is not None:
		
			filteredPicks, trashedpicks = filter_picks(imagePathBase, imageMask, self.pickInput, self.box, self.pickSuffix, paramOutSuffix=self.saveSuffix)

			if self.writeImage:
				apply_mask_and_picks(imagePathBase, filteredPicks, trashedpicks)

		elif self.writeImage:
			apply_mask(imagePathBase)

		return True

if __name__ == '__main__':

	AM = AutoMasker()
	parser, args = AM.parse_args()

	if (args.imageList and args.hole_diameter and args.apix and (not args.pickSuffix or (args.pickSuffix and (args.raw or args.appion_DoG or args.appion_template or args.raw_relion)))):
		AM.init_vars(**vars(args))
		AM.init_loop()
		AM.process_images()
	else:
		parser.print_help()

# End of File
