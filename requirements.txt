numpy>=1.11.0
Pillow>=3.2.0
scikit-image>=0.12.3
scipy>=0.17.1