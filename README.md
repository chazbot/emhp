# README #

EMHP is a set of tools for assessing cryo-EM images and filtering related single particle picks.  It runs using python 2.7, and supports the .mrc 2014 format for input images.

### How do I get set up? ###

* Setup process documented in [EMHP_Instructions.pdf](https://bitbucket.org/chazbot/emhp/downloads/EMHP_Instructions.pdf) in repository Downloads. 
* Sample data for testing located at [https://ward.scripps.edu/media/emhp_test_data.tar.gz](https://ward.scripps.edu/media/emhp_test_data.tar.gz) ~4.6Gb

### Who do I talk to? ###

* Contact repo owner or create an issue with any questions.