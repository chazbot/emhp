# Copyright 2017 The Scripps Research Institute

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## EMHP_picks.py
## Pick masker class extends ImageLoop to filter particle picks based on masks.
## bowman@scripps.edu

import os, sys, argparse
from math import ceil, sqrt

# Thanks for the parser AMI 8)
from pyami import mrc

import numpy as np
from PIL import Image
from scipy.ndimage.filters import gaussian_filter
from skimage.exposure import rescale_intensity
from skimage.filters import sobel
from scipy.misc import imresize#, imsave

from Utilities import *
from ImageLoop import ImageLoop


class PickMasker(ImageLoop):

	def __init__(self):
		ImageLoop.__init__(self)

	def parse_args(self):
		parser, required, optionalArgs, pickArgs = super(PickMasker, self).parse_args()

		optionalArgs.add_argument('--mask_suffix', dest='maskSuffix', metavar='SUFFIX', help='Suffix of pick file in same location as image file')
		optionalArgs.add_argument('--null_mask', dest='nullMask', metavar='WxH', help='use a null mask of size WxH - This just writes out all picks not on the border of the image')
		optionalArgs.add_argument('--also_null_mask', dest='alsoNullMask', metavar='WxH', help='pair with --mask_suffix to run null masks on images missing a mask file')

		args = parser.parse_args()
		return parser, args

	def init_vars(self, *args, **kwargs):
		super(PickMasker, self).init_vars(*args, **kwargs)

		# Force Single Thread
		self.multi = 1
		self.maskPool = None
		self.maskJobs = None

		# Optional Parameters
		self.maskSuffix = kwargs.get('maskSuffix', '_mask.png')
		self.nullMask = kwargs.get('nullMask', False)
		self.alsoNullMask = kwargs.get('alsoNullMask', False)

		if self.alsoNullMask and not self.maskSuffix:
			print_error("You should not use --also_null_mask without --mask_suffix, maybe you just meant to use --null_mask?")

		if self.nullMask:
			self.nullMask = [int(x) for x in self.nullMask.split('x')]
			print self.nullMask

		if self.alsoNullMask:
			self.alsoNullMask = [int(x) for x in self.alsoNullMask.split('x')]
			print self.alsoNullMask

		# Override log name
		self.logName = 'PickMasker_log.txt'
		self.logFile = os.path.join(self.logPath, self.logName)

	def process_image(self, paramImagePath):

		imagePathBase = os.path.splitext(paramImagePath)[0]

		if self.nullMask:
			imageMask = np.ones(self.nullMask)
		else:
			imageMaskPath = imagePathBase + args.maskSuffix
			if not os.path.isfile(imageMaskPath) and self.alsoNullMask:
				imageMask = np.ones(self.alsoNullMask)
			else:
				imageMask = np.asarray(Image.open(imageMaskPath)).astype(bool)
		
		if self.pickSuffix is not None and self.pickInput is not None:

			filteredPicks, trashedpicks = filter_picks(imagePathBase, imageMask, self.pickInput, self.box, self.pickSuffix, paramOutSuffix=self.saveSuffix)

			if self.writeImage and not self.nullMask:
				apply_mask_and_picks(imagePathBase, filteredPicks, trashedpicks)
			elif self.writeImage and self.nullMask:
				apply_picks(imagePathBase, filteredPicks, trashedpicks)

		elif self.writeImage and not self.nullMask:
			apply_mask(imagePathBase)

		return True

if __name__ == '__main__':

	PM = PickMasker()
	parser, args = PM.parse_args()

	if ((args.maskSuffix or args.nullMask) and \
		args.imageList and \
		(not (args.pickSuffix or args.maskSuffix) or (args.pickSuffix and (args.raw or args.appion_DoG or args.appion_template or args.raw_relion)))):
		PM.init_vars(**vars(args))
		PM.init_loop()
		PM.process_images()
	else:
		parser.print_help()

# End of File
