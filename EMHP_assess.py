# Copyright 2017 The Scripps Research Institute

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# imageAssessorTK.py
# A Tkinter implementation of the ImageAssessor superclass for image assessment
# bowman@scripps.edu

import os, sys, argparse

# Thanks for the parser AMI 8)
from pyami import mrc

from Utilities import *
from ImageLoop import ImageLoop

import pickle
import glob

from Tkinter import Tk, BOTH, LEFT, CENTER, IntVar
import tkMessageBox
from ttk import Frame, Button, Style, Label, Checkbutton

from PIL import Image, ImageTk

import numpy as np
from scipy.ndimage.filters import gaussian_filter
from skimage.exposure import equalize_adapthist, equalize_hist, rescale_intensity

import matplotlib.pyplot as plt

class ImageAssessorTK(ImageLoop):

	def __init__(self):
		ImageLoop.__init__(self)

		# Categories:
		# 0 Keep / Mask
		# 1 Keep / Manual Mask
		# 2 Keep / No Mask
		# 3 Discard
		self.categories = ["Keep / Mask      :", "Keep / Manual Mask", "Keep / No Mask   :", "Discard          :"]

		self.imageCategories = {}
		self.currentImage = None
		self.assessed = False

	# Completely override ImageLoop args
	def parse_args(self):

		parser = argparse.ArgumentParser(description='TEMHP Image Assessor\nby Charles Bowman\n Ward Lab - TSRI')

		# Required Arguments
		required = parser.add_argument_group("Required Arguments:")
		required.add_argument('--image_list', dest='imageList', metavar='PATH', help='Path to list of images to run autoMasker on (images should have absolute paths)')

		# Optional Arguments
		optionalArgs = parser.add_argument_group("Optional Arguments:")
		optionalArgs.add_argument('--scale', metavar='proportion', type=float, default=.25, help='scaling factor to apply to image in viewer')
		optionalArgs.add_argument('--filter', action='store_true', help='Low pass filter and histogram eq images')
		optionalArgs.add_argument('--resume', action='store_true', help='Resume your image assessment based on stored .pickle file')
		optionalArgs.add_argument('--log_path', default='', dest='logPath', metavar='PATH', help='Path to progress and log files, default is current folder')

		args = parser.parse_args()

		return parser, args

	# Completely override ImageLoop vars
	def init_vars(self, *args, **kwargs):
		
		# Required
		self.imageList = kwargs.get('imageList')

		# Optional
		self.resume = kwargs.get('resume', False)
		self.scale = kwargs.get('scale', .25)
		self.filter = kwargs.get('filter', False)
		self.logPath = kwargs.get('logPath', '')
		self.multi=1

		# Log File
		self.logName = 'log_TEMHP_assess.txt'
		self.logFile = os.path.join(self.logPath, self.logName)

		# Assessment info
		self.pickleName = 'log_TEMHP_assess_pickle.txt'
		self.pickleFile = os.path.join(self.logPath, self.pickleName)


	# Completely override loop initialization
	def init_loop(self):
		#Look for the file to open
		if os.path.isfile(str(self.imageList)):
			with open(str(self.imageList), 'r') as f:
				self.images = f.read().splitlines()
				f.close()
				self.total = len(self.images)
				self.count = 0
				if self.total < 1:
					print_error("Your image list is empty.")
				if self.resume:
					with open(self.pickleFile, 'r') as f: 
						self.imageCategories = pickle.load(f)
						self.complete = set(self.imageCategories.keys())
				else:
					self.complete = set()
					open(self.logFile, 'w').close()
		else:
			print_error("Could not open image list.")

	def _back(self):

		self.assessed = False

		print "Back             :"

		self.root.destroy()

	def _save_assessment(self, paramCategory):

		imagePathBase = os.path.splitext(self.currentImage)[0]

		print self.categories[paramCategory]

		# Save the assessment
		self.imageCategories[imagePathBase] = paramCategory
		with open(self.pickleFile, 'w') as f:
			pickle.dump(self.imageCategories, f)

		self.assessed = True

		self.root.destroy()

	def _toggle_image_display(self):

		im_ori = mrc.read(self.currentImage)
		width, height = im_ori.shape

		if self.filterIM.get() is 1:

			if self.contrastIM.get() is 1:
				image = equalize_hist(gaussian_filter(im_ori, sigma=3))
			else:
				p2, p98 = np.percentile(im_ori, (2, 98))
				image = rescale_intensity(gaussian_filter(im_ori, sigma=3), in_range=(p2, p98))

			image = Image.fromarray(np.uint8(image * 255)).resize((int(width * self.scale), int(height * self.scale)), Image.ANTIALIAS)
			photo = ImageTk.PhotoImage(image)

			self.labelImage.configure(image=photo)
			self.labelImage.image = photo

		elif self.filterIM.get() is 0:

			image = Image.fromarray(np.uint8(im_ori)).resize((int(width * self.scale), int(height * self.scale)), Image.ANTIALIAS)
			photo = ImageTk.PhotoImage(image)

			self.labelImage.configure(image=photo)
			self.labelImage.image = photo

	def process_image(self, paramImagePath):
		
		self.currentImage = paramImagePath
		self.assessed = False
		self.root = Tk()
		self.root.protocol("WM_DELETE_WINDOW", sys.exit)

		self.filterIM = IntVar()
		if self.filter:
			self.filterIM.set(1)

		self.contrastIM = IntVar()
		self.contrastIM.set(0)

		self.root.bind('<Left>', lambda event: self._back())
		self.root.bind('<Down>', lambda event, a=3: self._save_assessment(a))
		self.root.bind('<Up>', lambda event, a=2: self._save_assessment(a))
		self.root.bind('<Right>', lambda event, a=0: self._save_assessment(a))
		self.root.bind('<m>', lambda event, a=1: self._save_assessment(a))

		# Define Structure
		l = Frame(self.root, borderwidth=5)
		i = Frame(self.root, borderwidth=10)
		f = Frame(self.root, borderwidth=5)

		labelFilename = Label(i, text=self.currentImage)
		labelFilename.pack()

		self.labelImage = Label(i)
		self.labelImage.pack()

		b1 = Button(f,text="Back", command=lambda: self._back())
		b2 = Button(f,text="Trash", command=lambda: self._save_assessment(3))
		b3 = Button(f,text="Keep / No Mask", command=lambda: self._save_assessment(2))
		b4 = Button(f,text="Keep / Mask", command=lambda: self._save_assessment(0))
		b7 = Button(f,text="Keep / Manual Mask", command=lambda: self._save_assessment(1))
		c5 = Checkbutton(f,text="Filter Image", variable=self.filterIM, command=self._toggle_image_display)
		c6 = Checkbutton(f,text="Low Contrast Image", variable=self.contrastIM, command=self._toggle_image_display)
		b1.pack(side=LEFT,padx=5)
		b2.pack(side=LEFT,padx=5)
		b3.pack(side=LEFT,padx=5)
		b4.pack(side=LEFT,padx=5)
		b7.pack(side=LEFT,padx=5)
		c5.pack(side=LEFT,padx=5)
		c6.pack(side=LEFT,padx=5)

		l.pack(anchor=CENTER)
		i.pack()
		f.pack(anchor=CENTER)
		self._toggle_image_display()
		self.root.mainloop()

		return self.assessed

	def write_image_categories(self):

		# Categories:
		# 0 Keep / Mask
		# 1 Keep / Manual Mask
		# 2 Keep / No Mask
		# 3 Discard
		 
		zeroFile = open(os.path.join(self.logPath, "EMHP_auto_mask.txt"), 'w')
		oneFile = open(os.path.join(self.logPath, "EMHP_hand_mask.txt"), 'w')
		twoFile = open(os.path.join(self.logPath, "EMHP_no_mask.txt"), 'w')
		threeFile = open(os.path.join(self.logPath, "EMHP_trash.txt"), 'w')
		
		keepFile = open(os.path.join(self.logPath, "EMHP_keep_all.txt"), 'w')

		for key in self.imageCategories.keys():

			if self.imageCategories[key] is 0:
				zeroFile.write(key + ".mrc\n")
				keepFile.write(key + ".mrc\n")
			elif self.imageCategories[key] is 1:
				oneFile.write(key + ".mrc\n")
				keepFile.write(key + ".mrc\n")
			elif self.imageCategories[key] is 2:
				twoFile.write(key + ".mrc\n")
				keepFile.write(key + ".mrc\n")
			elif self.imageCategories[key] is 3:
				threeFile.write(key + ".mrc\n")
				
		zeroFile.close()
		oneFile.close()
		twoFile.close()
		threeFile.close()
		keepFile.close()

if __name__ == "__main__":
	
	IA = ImageAssessorTK()
	parser, args = IA.parse_args()

	if args.imageList:
		IA.init_vars(**vars(args))
		IA.init_loop()
		IA.process_images()
		IA.write_image_categories()
	else:
		parser.print_help()

# End of File
