# Copyright 2017 The Scripps Research Institute

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

## Utilities.py
## Methods here are shared between scripts and as such have been consolidated to calls for editing purposes.
## bowman@scripps.edu

#Python
import os, sys, pdb, traceback

# Thanks for the parser AMI 8)
from pyami import mrc

#Other
import numpy as np
from PIL import Image, ImageDraw
from scipy import misc
from scipy.ndimage.filters import gaussian_filter
from skimage.exposure import equalize_adapthist, equalize_hist, rescale_intensity

# From:  http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
# Print iterations progress
def print_progress(iteration, total, prefix = '', suffix = '', decimals = 2, barLength = 100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : number of decimals in percent complete (Int) 
        barLength   - Optional  : character length of bar (Int) 
    """
    filledLength    = int(round(barLength * iteration / float(total)))
    percents        = round(100.00 * (iteration / float(total)), decimals)
    bar             = '#' * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write('%s [%s] %s%s %s\r' % (prefix, bar, percents, '%', suffix)),
    sys.stdout.flush()
    if iteration == total:
        print("\n")

def print_error(paramErrorMsg):
	print 'ERROR: %s' % str(paramErrorMsg)
	print 'Please try again.'
	sys.exit()

def print_warning(paramErrorMsg):
	print 'WARNING: %s' % str(paramErrorMsg)
	print 'Please try again.'
	sys.exit()


# check if a particle potentially is on the edge of an image
# Input:
#	paramX - x coordinate of particle
#	paramY - y coordinate of particle
#	paramShape - shape of the mask image
# Output:
#	True if on edge, False otherwise
def _edge( paramX, paramY, paramShape, paramBox):

	if paramX < paramBox / 2:
		return True
	if paramX > paramShape[0] - paramBox / 2:
		return True
	if paramY < paramBox / 2:
		return True
	if paramY > paramShape[1] - paramBox / 2:
		return True

	return False

#Diagnostic Method for viewing masked image
# Input:
#	imagepath - path to image to load and display
#	mask - image mask binned by 10
#	picks - picks (output from _filter_picks) binned by 10
# Output:
def apply_mask_and_picks(paramImagePathBase, paramPicks, paramFiltered=[None, None]):

	image_raw = mrc.read(paramImagePathBase + '.mrc')

	p2, p98 = np.percentile(image_raw, (2, 98))
	
	image_adj = rescale_intensity(gaussian_filter(image_raw, sigma=3), in_range=(p2, p98))

	image_mask = np.transpose(np.asarray(Image.open(paramImagePathBase + '_mask.png')))

	final = np.clip(image_mask, 64, 255) * image_adj

	im = Image.fromarray(np.uint8(final))

	rgbim = Image.new("RGB", im.size)
	rgbim.paste(im)

	draw = ImageDraw.Draw(rgbim)

	for x, y in zip(paramPicks[0], paramPicks[1]):
		draw.ellipse((x-10,y-10,x+10, y+10) , outline='green', fill='green')

	for x, y in zip(paramFiltered[0], paramFiltered[1]):
		draw.ellipse((x-10,y-10,x+10, y+10) , outline='red', fill='red')

	rgbim.save(paramImagePathBase + "_filtered.jpg")

def apply_picks(paramImagePathBase, paramPicks, paramFiltered=[None, None]):

	image_raw = mrc.read(paramImagePathBase + '.mrc')

	p2, p98 = np.percentile(image_raw, (2, 98))
	
	image_adj = rescale_intensity(gaussian_filter(image_raw, sigma=3), in_range=(p2, p98))

	final = 255 * image_adj

	im = Image.fromarray(np.uint8(final))

	rgbim = Image.new("RGB", im.size)
	rgbim.paste(im)

	draw = ImageDraw.Draw(rgbim)

	for x, y in zip(paramPicks[0], paramPicks[1]):
		draw.ellipse((x-10,y-10,x+10, y+10) , outline='green', fill='green')

	for x, y in zip(paramFiltered[0], paramFiltered[1]):
		draw.ellipse((x-10,y-10,x+10, y+10) , outline='red', fill='red')

	rgbim.save(paramImagePathBase + "_filtered.jpg")

#Diagnostic Method for viewing masked image
# Input:
#	paramImagePathBase - path to image to load and display
# Output:
def apply_mask(paramImagePathBase):

	image_raw = mrc.read(paramImagePathBase + '.mrc')

	p2, p98 = np.percentile(image_raw, (2, 98))

	image_adj = rescale_intensity(gaussian_filter(image_raw, sigma=3), in_range=(p2, p98))

	image_mask = np.transpose(np.asarray(Image.open(paramImagePathBase + '_mask.png')))

	final = np.clip(image_mask, 64, 255) * image_adj

	im = Image.fromarray(np.uint8(final))
	im.save(paramImagePathBase + "_masked.jpg")


# Filters picks from pick files in masked regions - BINNED
# Input:
#	paramImageFileBase - base name of image file to use
#	paramImageMask - unBinned image mask
#	paramPickInput - denotes type of pick:
#		raw = x and y coordinates separated by whitespace
#		apDog = x and y in column 1 and 2
#		apTemplate = y and x in column 1 and 2
#	paramBox - box size of picked particles - used for edge filtering
#	paramPickSuffix - suffix used on base file name to load pick file
# Output:
#	returns the picks that werent filtered by this method with binned coordinates for plotting
def filter_picks(paramImageFileBase, paramImageMask, paramPickInput, paramBox, paramPickSuffix, paramWriteOut=True, paramOutSuffix="_picks_filtered.txt"):

	try:

		pickFile = paramImageFileBase + paramPickSuffix
		pickFileFinal = paramImageFileBase + paramOutSuffix
		pickFinal = []
		pickFinalX = []
		pickFinalY = []
		filteredx = []
		filteredy = []

		if os.path.isfile(str(pickFile)):
			with open(str(pickFile), 'r') as f:
				with open(str(pickFileFinal), 'w') as o:
					lines = f.read().splitlines()
					for line in lines:
						if line.startswith('#') or not line.strip():
							continue

						line = line.split()
						
						if paramPickInput == 'raw':
							x = int(float(line[0]))
							y = int(float(line[1]))

						elif paramPickInput == 'apDog' or paramPickInput == 'apTemplate':
							x = int(float(line[1]))
							y = int(float(line[2]))

						elif paramPickInput == 'rln':
							if line[0].startswith('_') or line[0].startswith('data_') or line[0].startswith('loop_'):
								continue
							else:
								x = int(float(line[0]))
								y = int(float(line[1]))

						if x >= paramImageMask.shape[0] or y >= paramImageMask.shape[1]:
							filteredx.append(x)
							filteredy.append(y)
							continue

						elif paramImageMask[x][y] and not (paramBox > 0 and _edge(x, y, paramImageMask.shape, paramBox)):
							pickFinalX.append(x)
							pickFinalY.append(y)
							o.write(str(x) + '\t' + str(y) + '\n')

						else:
							filteredx.append(x)
							filteredy.append(y)
							continue
				o.close()
			f.close()

		else:
			print_warning('Cannot find pick file: %s' % pickFile)

	except Exception, err:
		traceback.print_exc()
		#print line
		return


	return [pickFinalX, pickFinalY], [filteredx, filteredy]

def radial_sum(paramImage, paramRadius):
	radial_sum_return = np.zeros(paramImage.shape)
	for x in range(0, paramImage.shape[0]):
		if x < paramRadius:
			top = x
			bottom = paramRadius
		elif x > paramImage.shape[0] - paramRadius:
			top = paramRadius
			bottom = paramImage.shape[0] - x
		else:
			top = paramRadius
			bottom = paramRadius
		for y in range(0, paramImage.shape[1]):
			if y < paramRadius:
				left = y
				right = paramRadius
			elif y > paramImage.shape[1] - paramRadius:
				left = paramRadius
				right = paramImage.shape[1] - y
			else:
				left = paramRadius
				right = paramRadius
			radial_sum_return[x][y] = np.sum(paramImage[x - top:x + bottom, y - left:y + right])
	return radial_sum_return

# End of File