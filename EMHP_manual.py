## manualMasker.py
## Efficiently mask holes in cryoEM images and filter pick files based on masks - manually
## bowman@scripps.edu

import os, sys, argparse

from Tkinter import *
import tkMessageBox
from PIL import Image, ImageTk
import numpy as np
from scipy.misc import imresize #imsave, imresize
from scipy.ndimage.filters import gaussian_filter
from skimage.exposure import rescale_intensity, equalize_hist

from pyami import mrc

from Utilities import *
from ImageLoop import ImageLoop

class ManualMasker(ImageLoop):

	def __init__(self):
		ImageLoop.__init__(self)

	def parse_args(self):
		parser, required, optionalArgs, pickArgs = super(ManualMasker, self).parse_args()

		required.add_argument('--hole_diameter', metavar='INTEGER', type=int, help='Diameter of hole in carbon grid in Angstroms')
		required.add_argument('--apix', metavar='FLOAT', type=float, help='Pixel size in Angstroms')

		optionalArgs.add_argument('--scale', dest='imageScale', metavar='pix', type=float, default=.25, help='size of image to display in assessor')
		optionalArgs.add_argument('--low_contrast', dest='lowContrast',action='store_true', help='Apply filter for low contrast images ')
		
		args = parser.parse_args()
		return parser, args

	def init_vars(self, *args, **kwargs):
		super(ManualMasker, self).init_vars(*args, **kwargs)

		# Force Single Thread
		self.multi = 1
		self.maskPool = None
		self.maskJobs = None

		# Required Parameters
		self.hole_diameter = kwargs.get('hole_diameter')
		self.apix = kwargs.get('apix')
		
		# Optional Args
		self.imageScale = kwargs.get('imageScale', .25)
		self.lowContrast = kwargs.get('lowContrast', False)

		# Constants and Initial Values
		self.Rad = int((self.hole_diameter / self.apix) / 2)
		self.Rad_10 = int((self.hole_diameter / self.apix) / 20)

		self.circle_x = None
		self.circle_y = None
		
		self.marker_x = []
		self.marker_y = []

		self.pencil_x = []
		self.pencil_y = []

		self.currentMask = None
		self.low_perc = 1
		self.high_perc = 99
		self.drawn = [False, False, False]

	def _draw_circle(self, event):

		if self.drawn[0]:
			self.canvas.delete('holePunch')

		x1, y1 = ( event.x - self.Rad_10 * self.imageScale ), ( event.y - self.Rad_10 * self.imageScale )
		x2, y2 = ( event.x + self.Rad_10 * self.imageScale ), ( event.y + self.Rad_10 * self.imageScale )
		self.canvas.create_oval( x1, y1, x2, y2, outline = "#B22222", tags='holePunch' )
		self.drawn[0] = True
	
		self.circle_x = event.x
		self.circle_y = event.y

	def _draw_marker(self, event):
		x1, y1 = ( event.x - self.Rad_10 * self.imageScale * .05 ), ( event.y - self.Rad_10 * self.imageScale * .05 )
		x2, y2 = ( event.x + self.Rad_10 * self.imageScale * .05 ), ( event.y + self.Rad_10 * self.imageScale * .05 )
		self.canvas.create_oval( x1, y1, x2, y2, outline = "#22B222", fill = "#22B222", tags='marker' )
		self.drawn[1] = True

		self.marker_x.append(event.x)
		self.marker_y.append(event.y)

	def _draw_pencil(self, event):
		x1, y1 = ( event.x - self.Rad_10 * self.imageScale * .01 ), ( event.y - self.Rad_10 * self.imageScale * .01 )
		x2, y2 = ( event.x + self.Rad_10 * self.imageScale * .01 ), ( event.y + self.Rad_10 * self.imageScale * .01 )
		self.canvas.create_oval( x1, y1, x2, y2, outline = "#2222B2", fill = "#2222B2", tags='pencil' )
		self.drawn[2] = True

		self.pencil_x.append(event.x)
		self.pencil_y.append(event.y)

	def _draw(self, event):

		if self.paintBrush.get() == 1:
			self._draw_circle(event)
		elif self.paintBrush.get() == 2:
			self._draw_marker(event)
		elif self.paintBrush.get() == 3:
			self._draw_pencil(event)

	def _clear_canvas(self):

		self.canvas.delete('holePunch')
		self.canvas.delete('marker')
		self.canvas.delete('pencil')
		self.drawn = [False, False, False]

		self.circle_x = None
		self.circle_y = None

		self.marker_x = []
		self.marker_y = []

		self.pencil_x = []
		self.pencil_y = []

	def _back(self):

		self.currentMask = None

		self._clear_canvas()

		self.root.destroy()

	def _create_mask(self, paramImageShape):		

		if any(t for t in self.drawn):

			self.root.destroy()

			holePunchMask = np.transpose(np.zeros(paramImageShape))
			markerMask = np.transpose(np.zeros(paramImageShape))
			pencilMask = np.transpose(np.zeros(paramImageShape))

			I,J = np.meshgrid(np.arange(paramImageShape[1]),np.arange(paramImageShape[0]))
			
			if self.drawn[0]:

				ci = int(((self.circle_x / self.imageScale) - self.Rad_10) * 10)
				cj = int(((self.circle_y / self.imageScale) - self.Rad_10) * 10)

				holePunchMask = np.transpose(((I-ci)**2 + (J-cj)**2) > (self.Rad)**2)

			if self.drawn[1]:
				for x, y in zip(self.marker_x, self.marker_y):

					ci = int(((x / self.imageScale) - self.Rad_10) * 10)
					cj = int(((y / self.imageScale) - self.Rad_10) * 10)

					markerMask = np.logical_or(markerMask, np.transpose(((I-ci)**2 + (J-cj)**2) < (self.Rad * .05)**2))

			if self.drawn[2]:
				for x, y in zip(self.pencil_x, self.pencil_y):

					ci = int(((x / self.imageScale) - self.Rad_10) * 10)
					cj = int(((y / self.imageScale) - self.Rad_10) * 10)

					pencilMask = np.logical_or(pencilMask, np.transpose(((I-ci)**2 + (J-cj)**2) < (self.Rad * .01)**2))

		else:
			tkMessageBox.showwarning("No Mask Drawn", "Please click to add a circle mask.")

		self.currentMask = np.logical_not(np.logical_or(np.logical_or(holePunchMask, markerMask), pencilMask))

	def process_image(self, paramImagePath):
		
		# Initialize
		self.currentImage = paramImagePath
		self.root = Tk()
		self.root.protocol("WM_DELETE_WINDOW", sys.exit)

		# Load and preprocess mrcs
		im = mrc.read(paramImagePath)

		if self.lowContrast:
			im_eq = equalize_hist(gaussian_filter(im, sigma=3))
		else:
			im_eq = gaussian_filter(im, sigma=3)
			p1, p99 = np.percentile(im_eq, (self.low_perc, self.high_perc))
			im_eq = rescale_intensity(im_eq, in_range=(p1, p99))

		im_eq_dec = im_eq[0::10, 0::10]
		im_eq_dec_pad = np.pad(im_eq_dec, ((self.Rad_10,self.Rad_10),(self.Rad_10,self.Rad_10)), mode='constant')
		im_eq_dec_pad_scale = Image.fromarray(np.uint8(im_eq_dec_pad * 255)).resize((int(im_eq_dec_pad.shape[0] * self.imageScale), int(im_eq_dec_pad.shape[1] * self.imageScale)), Image.ANTIALIAS)
		im_final = ImageTk.PhotoImage(im_eq_dec_pad_scale)

		# Initialize GUI
		l = Frame(self.root, borderwidth=5)
		i = Frame(self.root)
		f = Frame(self.root, borderwidth=5)

		# Filename Label
		labelFilename = Label(l, text=paramImagePath)
		labelFilename.pack()

		# Canvas
		self.canvas = Canvas(i, width=int(im_eq_dec_pad.shape[0] * self.imageScale), height=int(im_eq_dec_pad.shape[1] * self.imageScale))
		self.canvas.pack()
		self.canvas.bind("<Button-1>", self._draw)
		self.canvas.create_image(0, 0, anchor=NW, image=im_final)

		# Buttons
		self.paintBrush = IntVar()
		self.paintBrush.set(1)
		rb1 = Radiobutton(f, text="Hole Punch", variable=self.paintBrush, value=1)
		rb2 = Radiobutton(f, text="Marker", variable=self.paintBrush, value=2)
		rb3 = Radiobutton(f, text="Pencil", variable=self.paintBrush, value=3)
		b1 = Button(f,text="Back", command=self._back)
		b2 = Button(f,text="Clear", command=self._clear_canvas)
		b3 = Button(f,text="Accept", command=lambda: self._create_mask(im_eq.shape))
		rb1.pack(side=LEFT,padx=5)
		rb2.pack(side=LEFT,padx=5)
		rb3.pack(side=LEFT,padx=5)
		b1.pack(side=LEFT,padx=5)
		b2.pack(side=LEFT,padx=5)
		b3.pack(side=LEFT,padx=5)

		# Pack
		l.pack(anchor=CENTER)
		i.pack()
		f.pack(anchor=CENTER)

		# Clear just in case
		self._clear_canvas()

		# Loop
		self.root.mainloop()

		# If root TK was killed and there is a mask process it
		if self.currentMask is not None:
			imagePathBase = os.path.splitext(paramImagePath)[0]

			# Fix for numpy 1.14 Bug:
			result = Image.fromarray((self.currentMask * 255).astype(np.uint8))
			result.save(imagePathBase + '_mask.png')			

			#imsave(imagePathBase + '_mask.png', self.currentMask)

			if self.pickSuffix is not None and self.pickInput is not None:
		
				filteredPicks, trashedpicks = filter_picks(imagePathBase, self.currentMask, self.pickInput, self.box, self.pickSuffix, paramOutSuffix=self.saveSuffix)

				if self.writeImage:
					apply_mask_and_picks(imagePathBase, filteredPicks, trashedpicks)

			elif self.writeImage:
				apply_mask(imagePathBase)
			
			self.currentMask = None
			return True

		# Otherwise user clicked back.
		else:
			return False

if __name__ == '__main__':
	MM = ManualMasker()
	parser, args = MM.parse_args()

	if (args.imageList and args.hole_diameter and args.apix and (not args.pickSuffix or (args.pickSuffix and (args.raw or args.appion_DoG or args.appion_template or args.raw_relion)))):
		MM.init_vars(**vars(args))
		MM.init_loop()
		MM.process_images()
	else:
		parser.print_help()
